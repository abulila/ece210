
%{
    Welcome to ECE210 Honors Section: Session #3. 

    Last session we practiced with vector and matrix indexing and a simple
    example of solution to linear equations by using the matrix inverse.

    This session we are going to learn some control flow: if, for, while
    through a simple circuit example.
%}

%%

%{
    To get help on a function, type the keyword, highlight it, right click
    and select "Help on Selection". If nothing shows up, try googling the
    keyword and Matlab.
%}

%%

%{
    Schedule:

    [Done] Week1: Matlab: Mathematical functions, Graphing tools, Vector and Matrix arithmetic
    [Done] Week2: Matlab: Vector and Matrix arithmetic, Solution to linear and differential equations
    Week3: Matlab: Control flow, iterative and vectorized solution
    Week4: Matlab: Defining functions, tips and tricks
%}


%% 04/10/2016: Matlab: Control flow, iterative and vectorized solution

%% Syntax:

%{
    if expression
       statements
    elseif expression
       statements
    else
       statements
    end

    ---
%
    for index = values
       program statements
              :
    end
 
    ---
%
    while expression
       statements
    end
%}

%% Exercise 1
%{
% Exercise 1: Referring to the attached .jpg document, make a plot of
% current i versus time t. Give appropriate axis labels and title. In
% addition, verify that your plot is correct by checking for the value of
% i(2)=1-exp(-2) and i(6)=(1-exp(-2))*exp(-1). Your figure should be
% similar to the one on Piazza.

% the circuit is the same as the circuit from HW4,
% the switch goes from position A to position B at time = 0
% the switch goes from position B to position A at time = 2

% general guide:
% define circuit parameters
% define initial conditions
% step through in time
% plot the results
%}
% Start by defining your circuit parameters R and L:
% <----- Here is space for you to complete this section ----->
R=1;
R2=0.25;
L=1;

% Initialize your variables
% initialize i(0), di/dt(0)
% di/dt is labeled as di in the code
% delta is labeled as dt in the code
% use a step size of 0.001
% <----- Here is space for you to complete this section ----->
i=0;
di=1/L;
dt=0.001;

% Stepping through time

index = 1;

for t = 0:dt:10
    
    % update variable i with di
    % specifically, i(t+dt) = i(t) + dt*di(t)
    
    % <----- Here is space for you to complete this section ----->
  
    i=i+di*dt;
    
    % update variable di with i
    % specifically, di(t+dt) = (1-i)/L if 0<t<2, (-iR)/L if t>2
    
    % <----- Here is space for you to complete this section ----->
    if(t<2)
        di=(1-i)/L;
    else
        di=-i*R2/L;
    end
    % saving the variables
    tout(index) = t;
    iout(index) = i;
    di_out(index) = di;
    
    index = index + 1;
end

% Plot the results

% <----- Here is space for you to complete this section ----->

plot(tout,iout)
xlabel('time')
ylabel('current')


%% Exercise 2 repeat Exercise 1 using a while loop

% <----- Here is space for you to complete this section ----->
R=1;
R2=0.25;
L=1;

i=0;
di=1/L;
dt=0.001;

index = 1;

while(t < 10)
   i = i+di*dt;
   if(t<2)
       di=(1-i)/L;
   else
       di=-i*R2/L;
   end
   
   tout(index) = t;
   iout(index) = di;
   di_out(index) = di;
   
   index = index + 1;
   t = t + dt;
end

plot(tout, iout)
xlabel('time')
ylabel('current')


%% Exercise 3: Approximate the value of pi

% Task 1:   Find a Converging Infinite Series that sums to pi 
%           https://en.wikipedia.org/wiki/List_of_formulae_involving_%CF%80#Other_infinite_series
% Task 2:   Find the sum using a for loop for N=100, plot the error of the 
%           summation (pi-estimate) against N (number of iterations)
% Task 3:   Find the number of iterations such that error < 1e-6, 
%           (hint: use a while loop and use a counter). Repeat for 1e-9

% <----- Here is space for you to complete this section ----->

N=1:100;
pi_estimate=zeros(1,length(N));
error=zeros(1,length(N));

for i=N
    
    sum = 0;
    for k = 0:i
        sum = sum + 1/(16^k) * ( 4/(8*k+1) - 2/(8*k+4) - 1/(8*k+5) - 1/(8*k+6) );
    end
    
    pi_estimates(i) = sum;
    error(i) = pi - sum;
    
end

plot(N, error)
xlabel('N')
ylabel('Error (pi-estimate)')

error_lt_6 = 0;
error_lt_9 = 0;

for i=N
    
    if error(i) > 1e-6
        error_lt_6 = error_lt_6 + 1;
        error_lt_9 = error_lt_9 + 1;
    elseif error(i) > 1e-9
        error_lt_9 = error_lt_9 + 1;
    else
        break
    end
    
end

error_lt_6
error_lt_9