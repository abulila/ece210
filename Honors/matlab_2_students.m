%{
    Welcome to ECE210 Honors Section: Session #2. Session 1 was tough but
    persevere and give it a chance (we will adjust the difficulty of
    subsequent sessions).

    Before we begin, make sure you open this document in Matlab editor
    instead of emacs/vi/gedit/... The cell mode should be automatically
    enabled and you will be able to execute each cell independently. To
    execute a cell, make sure the cell is selected and hit Ctrl Enter. Try
    it on this cell and the text "Welcome to ECE210 Honors Section: Session
    #2" should appear in your command window. If it does not, let the TA
    know.

    If you have not changed your shortcut preferences, do so now.
%}

disp('Welcome to ECE210 Honors Section: Session #2')

%%  

%{
    Help and Basics #2

    The symbol "%" is for comments.
    The symbol "%%" is used to divide your code into indivually executable sections. 

    To get help on a function, type the keyword, highlight it, right click
    and select "Help on Selection". If nothing shows up, try googling the
    keyword, Matlab.

    Get Ready and Have Fun!
%}

%%

%{
    Schedule:

    [Done] 13/09/2016: Matlab: Mathematical functions, Graphing tools, Vector and Matrix arithmetic
    27/09/2016: Matlab: Vector and Matrix arithmetic, Solution to linear and differential equations
    04/10/2016: Matlab: Control flow, iterative and vectorized solution
    11/10/2016: Matlab: Defining functions, tips and tricks
%}


%% 27/09/2016: Matlab: Vector and Matrix arithmetic, Solution to linear and differential equations

%% Exercise 1: Mathematical functions and Graphing tools

% Calculate and display the voltage, current and power passing through
% R_L for R_L = 1:1:20 kohm for the circuit displayed in
% Session2_DemoExercise.pdf question 1
%{
% hint: review matlab_1_students.m for
% how to define an array
% how to compute cell-by-cell operations between arrays
% how to create a figure
% how to draw multiple graphs on the same plot
% after using 'hold on', please remember to use 'hold off'.
% how to make appropriate grids, titles, labels
%}
% <----- Here is space for you to complete this section ----->

% t=0:0.01:10;
% y=sin(t);
% z=cos(t);
% hold on
% plot(t,y,'r:','DisplayName','y=sin(t)');
% plot(t,z,'b--','DisplayName','y=cos(t)');
% legend show

R_L = 1:1:20;
% I = 6 Volts /(5+R_L) kOhm
% KVL: -6V + 5kohm*I + R_L*I = 0
I = 6 ./ (5 + R_L);
V = 6 .* (R_L ./ (R_L + 5));
P = (I .^ 2) .* R_L;

plot(R_L, I,'DisplayName','Current (mA)');
hold on;
plot(R_L, V,'DisplayName','Voltage');
plot(R_L, P,'DisplayName','Power (mW)');
xlabel('R_L resistance (KOhms)');
ylabel('Voltage (V), Current (mA), or Power (mW)');
grid on
legend show
axis([0 20 0 5])

plot([3 3], [0 5], 'r--')
plot([6 6], [0 5], 'r--')
plot([18 18], [0 5], 'r--')



%% Exercise 2

% Most analog and digital filters (eg. audio filters, image filters) can be
% represented by a transfer function known as H(w). This will be covered in
% ECE210. H(w) is a complex-valued function with magnitude and phase. It is
% critical for most applications to determine the 3dB bandwidth. In the
% case of low pass filters, it is the angular frequency w_3dB where H(w) is
% half its peak value. 

% In this exercise, modify the following code such that the magnitude is
% displayed in logarithmic power scale = 20*log_base10(|H(w)|), draw a
% horizontal line intersection |H(w)| at the 3dB point.
%{
% Make a copy of the code before modification and be able to show us where
% you made the modification

% w = 0:0.01:10;
% H = 1./(1+1j*w);
% 
% subplot(2,1,1);
% plot(w,abs(H)); 
% title('Magnitude of H vs w');
% xlabel('w');
% ylabel('Magnitude'); 
% grid on;
% 
% subplot(2,1,2);
% plot(w,angle(H)); title('Phase of H vs w');
% xlabel('w');
% ylabel('Phase'); 
% grid on;
%}
w = 0:0.01:10;
H = 1./(1+1j*w);
H_db = 20 .* log10(abs(H));

subplot(2,1,1);
plot(w,H_db);
hold on
plot(w,ones(1001).*-3,'r--');
title('Magnitude of H vs w');
xlabel('w');
ylabel('Magnitude (dB)'); 
grid on;

subplot(2,1,2);
plot(w,angle(H)); title('Phase of H vs w');
xlabel('w');
ylabel('Phase'); 
grid on;



%% First things first Part 1, Indexing in Matlab (Vectors)
%{
% We shall learn about Matlab indexing, beginning with a row vector.
% Please execute the following commands and view the results in the command
% window.

% This initializes a row vector, since ';' was not used, the result is
% displayed in the command window. Similar to last week, you can also find
% the variable rv in your workspace.
rv = [10 12 13 14 16 14 17 14 20]

% To get the first element, second element, third element of the vector:
% So the syntax is <variable_name>(<element_index>)
rv(1)
rv(2)
rv(3)

% Different ways of getting the last element:
rv(end)
length(rv)        % finds out the length (number of elements) in rv
                  % use "help on selection" to find out more
rv(length(rv))

% Different ways of getting the second last element:
rv(end-1)
rv(length(rv)-1)

% Get all the odd index elements of rv :index 1,3,5,7,9
odd_index = [1,3,5,7,9]
odd_index_rv = rv(odd_index)

% Get all the odd elements of rv: 13, 17
odd_elements_rv = rv(find(mod(rv,2)==1))
% To understand what the above command is doing, execute the following
% and also use help on selection
mod(rv,2)==1
find(mod(rv,2)==1)
rv(find(mod(rv,2)==1))
% It is very important for you to understand this!
%}
%% First things first Part 2, Indexing in Matlab (Matrices)
%{
% Similar to the previous part, please execute the following commands and
% view the results in the command window.

% This initializes a matrix A with elements [1 2 3] in row 1, [4 5 6] in row 2.
% Matrix A should look like this:
%    11     12     13
%    14     15     16
A = [11 12 13; 14 15 16]

% To get the ith row, jth col element, use A(i,j), 
% For example, [2 row, 3 column] and [1 row, 2 column]
A(2,3)
A(1,2)

% To get the row and column dimensions of a matrix, use the command size
% The result is a vector
size(A)
% So to find out the number of rows, just index the first element of the
% results and to find out the number of cols, index the second element of
% the results.
size_ofA = size(A)
num_row_A = size_ofA(1)
num_col_A = size_ofA(2)

% Using the ':' operator in indexing means 'everything'
% eg. the entire first row of A = row 1, everything from row 1
row_1_A = A(1,:)
% eg. the entire third col of A = everything from col 3, col 3
col_3_A = A(:,3)

% Most vector indexing commands should also work on matrices
%}
%% Exercise 2: Indexing Questions

% 1) To get the second last element of a vector rv, a student did the following
%    Why is that wrong?
% --> rv(length(rv-1))

% 2) A student made voltage measurements of nodes 1, 2, 3, 4, 5, 6, 7, 8 as
voltages = [12, 0, 1, -3, -29, 30, 8, 21]

% Find the node corresponding to ground? (hint: find 0V)
% Find where did the largest voltage drop occur and what is it?

% 3) Draw a plausible tic-tac-toe diagram, put a '1' in the location where a
%    circle or a cross is drawn. eg. board_cross(2,2) = 1 puts a cross in the
%    middle of the board. make circles win, circles start first.

% <----- Here is space for you to complete this section ----->

% 1) using length(rv-1) is wrong because it subracts 1 from each element
%    of rv, rather than from the length.  Also, since MATLAB uses 1-indexing,
%    we can just use the length, since the 20th element of a 20 element array
%    will have the index of 20.
% --> rv(length(rv))

% 2)
gnd_node = find(voltages==0)
max_drop = max(diff(voltages))

% 3)
board = zeros(3,3)
board_cross = board;
board_circles = board; 

board_circles(1,1) = 1
board_cross(2,2) = 1
board_circles(3,3) = 1
board_cross(1,3) = 1
board_circles(3,1) = 1
board_cross(3,2) = 1
board_circles(2,1) = 1

current_board(:,:,1) = board;
current_board(:,:,2) = board_cross;
current_board(:,:,3) = board_circles;

imagesc(current_board) ; grid on; 

%% Final example of the day: Solution to linear equations
%{
clear all; close all; clc;

% Solving for unknown currents:

%{
    Given the following equations, please draw the circuit and show TA

    Loop1: -5V + 2I1 + 3(I1-I2) = 0
    Loop2: 3(I2-I1) + 2(I2) + 1(I2+1A) = 0

    Then look through the following steps carefully
%}

% Rearrange the loop equations so that I1 and I2 form columns
% Loop1: (2+3)*I1 + (-3)   *I2 = 5
% Loop2: (-3) *I1 + (3+2+1)*I2 = -1

% Loop1: (5)*I1 + (-3)*I2 = 5
% Loop2: (-3)*I1 + (6)*I2 = -1

% Convert the two linear equations into matrix form Ax = b
A = [5  -3
    -3   6]
b = [5
    -1]

% Invert matrix A and multiply it to both sides of the equation
% (Ainverse)*A*x = (Ainverse)*b
% (identity)  *x = (Ainverse)*b
%              x = (Ainverse)*b

currentsI1I2 = inv(A)*b

% Another way of doing this is:

currentsI1I2 = (A)\b
%}
%% Exercise 3: Final exercise of the day

% Calculate the currents I1 and I2 for the circuit displayed in
% Session2_DemoExercise.pdf question 2 using the method of matrix inverse

% <----- Here is space for you to complete this section ----->

a = [-3 1
     5 -9]
b = [2
     0]

inv_a = inv(a)

currents = inv_a * b

