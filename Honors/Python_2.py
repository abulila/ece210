import numpy as np

def distance(a, b):
    return np.sqrt(np.dot(b-a, b-a))

def direction(a, b):
    return (b-a)/distance(a,b)

points = {'a':np.array([0,0,0]),    \
          'b':np.array([1,1,1]),    \
          'c':np.array([1,-1,0]),   \
          'd':np.array([-5,10,2]),  \
          'e':np.array([1,3,1])}

format_string = 'point {0} to point {1}, distance: {2:.2f}\tdirection: {3: .2f}, {4: .2f}, {5: .2f}'

for key1 in sorted(points.keys()):
    for key2 in sorted(points.keys()):
        if key1 == key2:
            continue
        point1, point2 = points[key1], points[key2]
        print(format_string.format(key1, key2, distance(point1,point2), *direction(point1, point2)))
