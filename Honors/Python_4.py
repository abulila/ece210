import numpy as np
import matplotlib.pyplot as plt
import numpy.fft as fft
import scipy.io.wavfile as wav
import math

# # Example 1
# f = 300.0
# fs_1 = 4000.0
# fs_2 = 1000.0
# fs_3 = 500.0
#
# t1 = np.arange(0, 0.02, 1/fs_1)
# t2 = np.arange(0, 0.02, 1/fs_2)
# t3 = np.arange(0, 0.02, 1/fs_3)
# y1 = np.sin(2 * np.pi * f * t1)
# y2 = np.sin(2 * np.pi * f * t2)
# y3 = np.sin(2 * np.pi * f * t3)
#
# fig, axes = plt.subplots(nrows=3, ncols=1)
#
# axes[0].plot(t1, y1, 'r.-')
# axes[0].set_xlabel('t')
# axes[0].set_ylabel('y')
# axes[1].plot(t2, y2, 'b.-')
# axes[1].set_xlabel('t')
# axes[1].set_ylabel('y')
# axes[2].plot(t3, y3, 'g.-')
# axes[2].set_xlabel('t')
# axes[2].set_ylabel('y')
#
# fig.tight_layout()
# plt.show()

# Example 2
fs = 8000.0
f_1 = 500.0; phase_1 = 0
f_2 = 750.0; phase_2 = np.pi/4.0
f_3 = 1500.0; phase_3 = np.pi/2.0
f_4 = 4500.0; phase_4 = 0
t = np.arange(0,1,1/fs)
y = 1
y += np.cos(2 * np.pi * f_1 * t + phase_1)
y += 0.5 * np.cos(2 * np.pi * f_2 * t + phase_2)
y += np.cos(2 * np.pi * f_3 * t + phase_3)
y += np.cos(2 * np.pi * f_4 * t + phase_4)
# plt.plot(t[0:80],y[0:80],'r.-')
# plt.show()

N = 1024
y_spec = fft.fft(y,N)
ff = np.linspace(-fs/2,fs/2,N)
y_spec_mag = abs(fft.fftshift(y_spec))
y_spec_phase = np.angle(fft.fftshift(y_spec))

fig, axes = plt.subplots(nrows=1, ncols=1)
axes.plot(ff, y_spec_mag, 'r')
axes.set_xlabel('frequency')
axes.set_ylabel('magnitude')

fig.tight_layout()
plt.show()

### Question: Interpret the plot
# This plot shows the magnitude of each cosine function that
# is part of the sum that defines y(t).  i.e. the Fourier coefficients
# This basically shows which frequencies can be addded together
# to get the original signal

# ## FFT Exercise
# [fs,y] = wav.read('Happy_music_mono.wav')
# print(fs)
# n = y.shape[0]
# print(n)
# t = np.linspace(0.0, n/fs, n)
# fig, axes = plt.subplots(nrows=2, ncols=1)
# axes[0].plot(t,y,'b')
# axes[0].set_xlabel('time(s)')
# axes[0].set_ylabel('magnitude')
#
# # Perform FFT on the signal with N=4096
# n_fft = 4096
# y_spec = fft.fft(y,n_fft)
# ff = np.linspace(-fs/2, fs/2, n_fft)
# y_spec_mag = abs(fft.fftshift(y_spec))
# y_spec_db = 20 * np.log10(y_spec_mag)
# y_spec_phase = np.angle(fft.fftshift(y_spec))
#
# axes[1].plot(ff[2048:], y_spec_mag[2048:], 'r')
# # axes[1].plot(ff[2048:], y_spec_db[2048:], 'r')
# axes[1].set_xlabel('frequency')
# axes[1].set_ylabel('magnitude')
# # axes[1].set_ylabel('magnitude (dB)')
#
# fig.tight_layout()
# plt.show()
