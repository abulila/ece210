import matplotlib.pyplot as plt
import numpy as np

x = np.arange(0, 10, 0.01)

y1 = np.cos(x**2)
y2 = np.sin(x)
y3 = y2 - y1


fig1 = plt.figure(num = 1)
ax1 = fig1.gca()

# fig1, ax = plt.subplots(nrows = 3, ncols = 1)
# ax1 = ax[0]
# ax2 = ax[1]
# ax3 = ax[2]

ax1.plot(x, y1, 'r:', label = 'y=cos(x^2)')
ax1.hold(True)
ax1.set_xlabel('x')
ax1.set_ylabel('y')

ax1.plot(x, y2, 'b--', label = 'y=sin(x)')
# ax2.set_xlabel('x')
# ax2.set_ylabel('y')

ax1.plot(x, y3, 'g', label = 'Diff')
# ax3.set_xlabel('x')
# ax3.set_ylabel('y')

# ax1.set_title('y=cos(x^2)')
# ax2.set_title('y=sin(x)')
# ax3.set_title('y=sin(x) - cos(x^2)')

handles, labels = ax1.get_legend_handles_labels()

ax1.legend(handles, labels, loc = 'best')
fig1.tight_layout()
# plt.grid()
plt.show()
