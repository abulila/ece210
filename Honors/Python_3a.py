class Car:
    def __init__(self, name, price, insurance):
        self.name = name
        self.price = price
        self.insurance = insurance
    def collision(self, other_car):
        print('Insurance for {0} currently costs ${2:.2f}, and the car is worth ${1:.2f}'.format(self.name, self.price, self.insurance))
        self.insurance += other_car.insurance
        self.price *= .9
        print('** CRASH!! **')
        print('Insurance for {0} now costs ${2:.2f}, and the car is worth ${1:.2f}\n'.format(self.name, self.price, self.insurance))


car1 = Car('Sheila', 20000, 500)
car2 = Car('Bertha', 15860, 250)

car1.collision(car2)
