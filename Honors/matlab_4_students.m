%{
    Welcome to ECE210 Honors Section: Session #4. 

    Last session we practiced with control flow: if, for, while
    through a simple circuit example. And also reviewed plotting.

    This session we will learn how to write functions and review plotting.
    Plotting is quite important because we want to be able to visualize and
    make sense of the results that we get from our experiments.
%}

%% 

%{
    Eg. x = linspace(0,2*pi,100); y1 = sin(x); y2 = cos(x);
    
    Syntax for plot in matlab:

    - plot(X1,Y1) 
        plot(x,y1)

    - plot(X1,Y1,LineSpec)
        plot(x,y1,''b.')
        this plots each point in x,y1 as blue dots

    - plot(X1,Y1,LineSpec,'PropertyName',PropertyValue)
        plot(x,y1,'r:','DisplayName','y1 is sin(x)')
        this plots each point in x,y1 joined by red dotted lines
        and also gives it the label of 'y1 is sin(x)' when you use legend

    - plot(X1,Y1,LineSpec,...,Xn,Yn,LineSpec)
    - plot(...,'PropertyName',PropertyValue,...)

%}

%%

%{
    Schedule:

    [Done] 13/09/2016: Matlab: Mathematical functions, Graphing tools, Vector and Matrix arithmetic
    [Done] 27/09/2016: Matlab: Vector and Matrix arithmetic, Solution to linear and differential equations
    [Done] 04/10/2016: Matlab: Control flow, iterative and vectorized solution
    11/10/2016: Matlab: Defining functions, tips and tricks
%}

%% 30/09/2014: Matlab: Defining functions, tips and tricks

%% Exercise 1: Review of plotting lines and find

%  f(theta) = sin(theta) for theta in the range [0,2*pi]
%  Find the range of theta = [theta1,theta2] for which f(theta) > 0.5
%  In figure 133,
%  Plot f(theta) for theta not in the range [theta1,theta2]as black -- lines,
%       note that this range of theta has two parts
%  Plot f(theta) for theta in the range [theta1,theta2] as red -. lines.
%  Mark f(theta) = 0.5 with a dotted blue horizontal line,
%  Mark theta = theta1 and theta = theta2 with dotted green vertical lines
%  Make sure axis are of the right limits
%  Give appropriate labels, title, legend, use the greek symbol of theta.
%  Suppress all long outputs from command window.

%  Will not be going through in class how to do this exercise because by
%  this point in time, you should be able to accomplish this. This also
%  means that we will not be answering any questions related to this
%  specific exercise.

%  Hint for find function: first come out for a logical expression that
%  returns true or false for each element in the array, then use the find
%  command to return the indexes of the elements which are true.

%  Hint for horizontal line: find the start point, find the end point,
%  express them in a vector of x-coordinates and another vector of
%  y-coordinates. same hint for vertical line. 

%  Hint for labeling, use '\theta' in place of 'theta'

%  An example that satisfy all the requirements above is available on
%  Piazza

% <----- Here is space for you to complete this section ----->

x = linspace(0,2*pi,100);
y = sin(x);
theta1 = (find(y >= 0.5, 1, 'first') - 1) * (2*pi/100);
theta2 = find(y >= 0.5, 1, 'last') * (2*pi/100);

% indicies_lt = find(y <= 0.5);
% indicies_gt = find(y >= 0.5);

x1 = linspace(0, theta1, 100);
x2 = linspace(theta1, theta2, 100);
x3 = linspace(theta2, 2*pi, 100);

hold on;
plot(x1, sin(x1),'k--', x2, sin(x2), 'r-.', x3, sin(x3), 'k--');
% plot(x(indicies_lt), y(indicies_lt), 'k--');
% plot(x(indicies_gt), y(indicies_gt), 'r-.');

plot([0,2*pi],[0.5,0.5],'b:','LineWidth',1.5);
plot([theta1,theta1],[-1.5,1.5],'g:','LineWidth',1.5);
plot([theta2,theta2],[-1.5,1.5],'g:','LineWidth',1.5);
axis([0,2*pi,-1.5,1.5])
xlabel('\theta');
ylabel('sin(\theta)');
legend('sin(\theta) < 0.5', 'sin(\theta) > 0.5');
title('Plot of sin(\theta) vs \theta showing values greater than 0.5');


% if your \theta and \pi turns out as q and p in linux, it is ok but we
% would require you to show us that the q and p is actually \theta and \pi


%% What is a function?

%{
    A function has a name so that you can call it by its name
    A function may take some inputs in the parenthesis on the right
    A function may give some outputs before the equality sign on the left
    It is like one of the black boxes that you define in ECE210

    Please read the following very carefully:

    Syntax: 
    function [y1,...,yN] = myfun(x1,...,xM)

    Description:
    function [y1,...,yN] = myfun(x1,...,xM) declares a function named myfun
    that accepts inputs x1,...,xM and returns outputs y1,...,yN. This
    declaration statement must be the first executable line of the
    function.

    Save the function code in a text file with a .m extension. The name of
    the file should match the name of the first function in the file. Valid
    function names begin with an alphabetic character, and can contain
    letters, numbers, or underscores.

    You can declare multiple local functions within the same file, or nest
    functions. If any function in a file contains a nested function, all
    functions in the file must use the end keyword to indicate the end of
    the function. Otherwise, the end keyword is optional.
%}

%% Example: Function Definition

%  Create a new function file, save it as myphase.m

%  Copy the following definition into the file:
%{
    function [ z_phase ] = myphase( z )
    % This function returns the phase of the complex number z

    z_phase = angle(z);

    end
%}

%   Save the file and execute the following commands:

    z = 1+j;
    z_phase = myphase(z)



%% Exercise 2: Function Definition

%  Define a function names myHW5Qn5 that will:
%  1. Take the inputs w and F, 
%     w : input frequency, F: phasor (complex number)
%  2. Print the cosine function for the input frequency and phasor
%     eg. w = 5rad/s, F = 2*exp(-j*pi/3),
%        disp(['substringA','substringB','substringC',...]) that results in 
%        'For w = 5rad/s, F = 1.0000 - 1.7321i, Answer: 2cos(5t-1.0472)' 
%        being displayed in command window
%     hint: use num2str to convert a number into a string
%  3. Output the magnitude and phase of the phasor as z_mag and z_phase

%  Do the following in checkpoints for easy debugging

%  Checkpoint1: created function, able to call function
%  Checkpoint2: added inputs to the function, function displays inputs
%  Checkpoint3: added outputs to the function, function returns outputs
%  Checkpoint4: function displays the correct statement in command window

%  Execute this for w = 5rad/s, 
  F1 = 2*exp(-j*pi/3)
  F2 = -sqrt(3)*(1+j)
  F3 = -2 -j*sqrt(3) + 3*exp(j*pi/3)
%  check against you homework solutions


% <----- Here is space for you to complete this section ----->

myHW5Qn5(5,F1);
myHW5Qn5(5,F2);
myHW5Qn5(5,F3);



%% Exercise 3: Show that you have completed the successful installation of python
% See how easy this assignment is!
