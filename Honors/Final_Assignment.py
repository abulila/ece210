#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
import numpy.fft as fft
import scipy.signal as sig

# Main Menu
def main():
    is_running = True
    input()
    while is_running:

        print('ECE 210 Honors Section Final Assignment')
        print('=======================================')
        print('Chose a demo to run...')
        print('  0) Quit')
        print('  1) Projectile Motion')
        print('  2) Estimating Pi')
        print('  3) Amplitude Modulation + FFT')
        print('  4) Digital Filtering w/ FFT')
        choice = int(input('>>> '))


        if choice == 1:
            print('... Running Projectile Demo\n')
            demo_projectile()
        elif choice == 2:
            print('... Running Pi Demo\n')
            demo_pi()
        elif choice == 3:
            print('... Running AM Demo\n')
            demo_am()
        elif choice == 4:
            print('... Running Filter Demo\n')
            demo_filter()
        else:
            print('... Quitting')
            is_running = False

    plt.close('all')


# Question 1
def demo_projectile():
    # List of positions for each theta
    # [theta, X[], Y[]]
    position = []

    for theta in range(15, 80, 10):
        # constants
        v = 100     # with no drag, v doesn't change
        a = -9.81
        m = 1       # not needed for these calculations
        timestep = .1

        # Initial conditions
        t = 0
        x = [0]
        y = [0]
        vx = v * np.cos(np.radians(theta))      # vx is constant (no drag)
        vy = v * np.sin(np.radians(theta))      # vy will change due to gravity

        # step through time, building x[], y[]
        while y[-1] >= 0:
            x.append( x[-1] + vx * timestep )
            y.append( y[-1] + vy * timestep )
            vy += a * timestep

        # store x and y arrays in position list with theta
        position.append([theta, x, y])

    # plot x and y for each value of theta
    fig = plt.figure()
    ax = fig.gca()

    ax.set_xlabel('x (m)')
    ax.set_ylabel('y (m)')
    ax.set_title('Projectile Motion for Different Angles')

    for item in position:
        ax.plot(item[1], item[2], label = 'theta = {}'.format(item[0]))

    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles, labels, loc = 'best')

    fig.tight_layout()
    plt.show()

    print('... Finished Projectile Demo\n')


# Question 2
def demo_pi():
    # estimate pi using the Monte Carlo method
    def monte_carlo(num):
        count = 0
        coords = np.random.rand(num, 2)
        for point in coords:
            x = point[0]
            y = point[1]
            if np.sqrt(x**2 + y**2) <= 1:
                count += 1
        return 4 * count/num

    # estimate pi using the Leibniz formula
    # pi/4 = 1 - 1/3 + 1/5 - 1/7 + 1/9 - ...
    def leibniz(num):
        total = 0
        for k in range(num):
            total += (-1)**k / (2*k+1)
        return 4 * total

    n = [100, 200, 500, 1000, 2000, 5000, 10000, 100000]
    pi_mc = []      # Array of values calculated using the Monte Carlo method
    pi_iter = []    # Array of values calculated using an iterative method

    # Print the calculated values as we go so we can see what's happening
    print('Monte Carlo Method:')
    for number in n:
        pi_approx = monte_carlo(number)
        pi_mc.append(pi_approx)
        print('n = {}, pi \u2248 {}'.format(number, pi_approx))
    print('\nIterative Method:')
    for number in n:
        pi_approx = leibniz(number)
        pi_iter.append(pi_approx)
        print('n = {}, pi \u2248 {}'.format(number, pi_approx))

    # Calculate errors for each iteration against accepted value of Pi
    pi_error_mc = [abs(np.pi - approx) for approx in pi_mc]
    pi_error_iter = [abs(np.pi - approx) for approx in pi_iter]

    # Plot both approximations on a log-log graph
    fig = plt.figure()
    ax = fig.gca()

    ax.set_title('Approximating Pi')

    ax.plot(n, pi_error_mc, label = 'Monte Carlo Method')
    ax.plot(n, pi_error_iter, label = 'Iterative Method')
    ax.set_xscale('log')
    ax.set_yscale('log')

    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles, labels, loc = 'best')

    fig.tight_layout()
    plt.show()

    print('... Finished Pi Demo\n')


# Question 3
def demo_am():
    # define the frequencies
    f_sample = 192000   # 192 kHz
    f_carrier = 8000    # 8 kHz
    f_signal = 1000     # 1 kHz

    # defince the periods
    T_sample = 1/f_sample
    T_carrier = 1/f_carrier

    t = np.arange(0, 10/f_carrier, T_sample)

    # define the 8kHz carrier signal and the 1kHz message signal
    carrier = np.cos(2 * np.pi * f_carrier * t)
    signal = np.cos(2 * np.pi * f_signal * t)
    # create a modulated waveform by scaling the signal by the modulation
    # depth (50%) and adding a DC offset to make the signal positive, then
    # mixing it with the carrier.
    modulation = np.multiply(carrier, np.add(1, 0.5*signal))

    # perform an FFT on carrier and modulated signals
    N = 1024
    carrier_spectrum = abs(fft.fftshift(fft.fft(carrier, N)))
    modulation_spectrum = abs(fft.fftshift(fft.fft(modulation, N)))
    f_k = np.linspace(-f_sample/2, f_sample/2, N)

    # plot the time domain waveforms
    plt.figure(1)
    plt.subplot(221)
    plt.plot(t, carrier)
    plt.title('Carrier Signal')
    plt.subplot(223)
    plt.plot(t, modulation)
    plt.title('Modulated Signal')

    # plot the frequency domain spectra
    plt.subplot(222)
    plt.plot(f_k, carrier_spectrum)
    plt.title('Carrier Spectrum')
    plt.subplot(224)
    plt.plot(f_k, modulation_spectrum)
    plt.title('Modulated Spectrum')

    plt.show()

    print('Finished AM Demo')


# Question 4
def demo_filter():
    f_sample = 32000    # 32 kHz
    t = np.linspace(0, 0.032, 1024)    # time vector of length 1024 at frequency 32kHz

    y1 = 10 * sig.square(2 * np.pi * 1000 * t)
    y2 = np.cos(2 * np.pi * 1000 * t + np.pi/2)
    y3 = np.cos(2 * np.pi * 10000 * t + np.pi/4)
    y4 = y1 + y2 + y3

    N = 1024
    f_k = np.linspace(-f_sample/2, f_sample/2, N)
    spectrum1 = abs(fft.fftshift(fft.fft(y1, N)))
    spectrum2 = abs(fft.fftshift(fft.fft(y2, N)))
    spectrum3 = abs(fft.fftshift(fft.fft(y3, N)))
    spectrum4 = abs(fft.fftshift(fft.fft(y4, N)))

    lpf = [0] * 1024
    hpf = [1] * 1024
    for i in range(256,768):
        lpf[i] = 1    # Low Pass = 1 for -fs/4 < w < fs/4
        hpf[i] = 0    # High Pass = 1 for w < -fs/4 and w > fs/4

    highpass1 = spectrum1 * hpf
    highpass2 = spectrum2 * hpf
    highpass3 = spectrum3 * hpf
    highpass4 = spectrum4 * hpf
    lowpass1 = spectrum1 * lpf
    lowpass2 = spectrum2 * lpf
    lowpass3 = spectrum3 * lpf
    lowpass4 = spectrum4 * lpf

    lp_y1 = fft.ifft(fft.fftshift(lowpass1), N)
    lp_y2 = fft.ifft(fft.fftshift(lowpass2), N)
    lp_y3 = fft.ifft(fft.fftshift(lowpass3), N)
    lp_y4 = fft.ifft(fft.fftshift(lowpass4), N)
    hp_y1 = fft.ifft(fft.fftshift(highpass1), N)
    hp_y2 = fft.ifft(fft.fftshift(highpass2), N)
    hp_y3 = fft.ifft(fft.fftshift(highpass3), N)
    hp_y4 = fft.ifft(fft.fftshift(highpass4), N)

    # plot the time domain signals
    plt.figure(1)
    plt.subplot(411)
    plt.plot(t[:100],y1[:100])
    plt.subplot(412)
    plt.plot(t[:100],y2[:100])
    plt.subplot(413)
    plt.plot(t[:100],y3[:100])
    plt.subplot(414)
    plt.plot(t[:100],y4[:100])

    # plot the frequency domain spectra
    plt.figure(2)
    plt.subplot(411)
    plt.plot(f_k, spectrum1)
    plt.subplot(412)
    plt.plot(f_k, spectrum2)
    plt.subplot(413)
    plt.plot(f_k, spectrum3)
    plt.subplot(414)
    plt.plot(f_k, spectrum4)

    # plot the filter reponses
    plt.figure(3)
    plt.subplot(211)
    plt.plot(f_k, lpf)
    plt.subplot(212)
    plt.plot(f_k, hpf)

    # plot the lowpass filtered outputs
    plt.figure(4)
    plt.subplot(411)
    plt.plot(t[:100],lp_y1[:100])
    plt.subplot(412)
    plt.plot(t[:100],lp_y2[:100])
    plt.subplot(413)
    plt.plot(t[:100],lp_y3[:100])
    plt.subplot(414)
    plt.plot(t[:100],lp_y4[:100])

    # plot the highpass filtered outputs
    plt.figure(5)
    plt.subplot(411)
    plt.plot(t[:100],hp_y1[:100])
    plt.subplot(412)
    plt.plot(t[:100],hp_y2[:100])
    plt.subplot(413)
    plt.plot(t[:100],hp_y3[:100])
    plt.subplot(414)
    plt.plot(t[:100],hp_y4[:100])

    plt.show()

    print('Finished Filter Demo')

if __name__ == '__main__':
    main()
