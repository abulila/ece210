%{
    Welcome to ECE210 Honors Section. There will be 8 short tutorials in
    total: 4 for MATLAB and 4 for Python. You will have to complete all 8
    tutorials in order to gain Honors credit for this section.

    Before we begin, most of us are more familiar with the Windows set of
    keyboard shortcuts rather than the emacs set of keyboard shortcuts. So,
    let us change our shortcut preferences. Under the toolstrip, located at
    the top of the MATLAB window, search for "Environment" and click
    "Preferences". In the new "Preferences" window, select "Keyboard".
    Under "Keyboard", click on the option called "Shortcuts". On the right,
    under "Active Settings", please select "Windows Default Set". Finally,
    click "Apply" and "Ok".
%}

%%

%{
    Help and Basics

    The symbol "%" is used in front of a comment. Comments are usually
    color-coded green. Use "%%" to separate your code, typically written in
    a script (.m file) into individually executable sections. You may have
    to save your error-free script as a .m file in order for the
    color-coding and section separation to activate.

    To get a list of help topics, type "help" and hit enter/return. To get
    specific help for a function eg. sin, type "help sin" and hit
    enter/return. Since it is tedious to keep typing "and hit
    enter/return", we shall omit it from future instructions.

    If you don't know the exact name of the topic or command you are
    looking for, type "lookfor keyword" (e.g., "lookfor regression")

    When writing a long matlab statement that exceeds a single row use
    "..." to continue statement to next row.

    When using the command line, a ";" at the end means matlab will not
    display the result. If ";" is omitted then matlab will display result.
    It is a good practice to use ";" to suppress most results. 

    Use the up-arrow to recall commands without retyping them (and down
    arrow to go forward in commands). Tab-completion is also available.

    Some useful commands for clearing variables and windows
    clear all;	% clear all variables
    close all;	% close all additional windows
    clc;        % clears command window
    disp('<insert message here>'); % same as print statements

    Get Ready and Have Fun!

%}

%%

%{
    Schedule:

    13/09/2016: Matlab: Mathematical functions, Graphing tools, Vector and Matrix arithmetic
    27/09/2016: Matlab: Vector and Matrix arithmetic, Solution to linear and differential equations
    04/10/2016: Matlab: Control flow, iterative and vectorized solution
    11/10/2016: Matlab: Defining functions, tips and tricks
%}


%% 13/09/2016: Matlab: Mathematical functions, Graphing tools, Vector and Matrix arithmetic

%% First things first, Declaring Vectors and Matrices

% Declaring a row vector rv with elements [1 2 3 4 6 4 3 4 5], look at the
% form of rv being displayed in the Command Window. rv is also displayed in
% the Workspace, double-click on rv in the workspace and you will be able
% to view the entries of rv in a spreadsheet.
rv = [1 2 3 4 6 4 3 4 5]

% Declare a column vector cv with the same elements as rv. Observe the form
% of cv in the Command Window. In addition, the "'" operator is able to
% give you the transform of a matrix/ vector.
cv = [1;2;3;4;6;4;3;4;5]
new_rv = cv'
new_cv = rv'

% Declaring a vector of time t1 from 0s to 100s with 0.1s increments,
% declare another vector of time t2 from 0 to 100 with exactly 574
% intervals.
t1 = 0:0.1:100
t2 = linspace(0, 100, 574)

% Declaring a matrix A with elements [1 2 3] in row 1, [4 5 6] in row 2.
A = [1 2 3; 4 5 6]

% Declaring a matrix Z of size 4x20 with entries 0, declare another matrix
% O of size 5x3 with entries 4.
Z = zeros(4,20)
O = ones(5,3)*4

% Practice declaring a few vectors/ matrices until you get the feel of it.

%% Interesting numbers

% Do explore and take note of the following results
% NaN represents "Not a Number"
a0 = 1/inf
a1 = 1/0
a2 = inf/0      % do take note
a3 = 0/0        % do take note
a4 = inf^inf
a5 = inf - inf  % do take note
b1 = i
b2 = j
b3 = sqrt(-1)
b4 = b1*b2
b5 = NaN

%% Mathematical functions and Graphing tools

%{
    In this example, we defined, plotted and sounded out a sine wave of
    400Hz sampled at 8192Hz. We do recommend that you go through this
    example in detail and it would be a good idea to put on your earphones
    at this point. After analyzing this example, write a 1.5 seconds sine
    sweep from 400Hz to 800Hz.
%}

% Declaring time t starting from 0, ending at 1s, with sampling frequency
% of 8192 Hz, hence period of 1/8192s. This is the default sampling
% frequency for the command soundsc.
t = 0:1/8192:1;

% Declaring the sine wave with frequency 400Hz, hence angular frequency of
% 2*pi*400 rad. Knowing how to write sine waves is actually quite important
% in ECE210 as we tend to do frequency analysis with sinusoidal inputs sin(wt)
sinewave = sin(2*pi*400*t);

% This is the typical way of plotting a graph and we would be expecting this of you.
% Firstly, create a new "figure" and label it with a positive integer. 
% Then, use "plot" which is the command to graph the sinewave in 2D.
% Please label your axis with "xlabel", "ylabel" and give your figure an appropriate "title".
% Adjust your "axis" so that the graph makes sense.
% If you need help with any of the commands at any point, highlight the command,
% right click and select "Help on Selection"
figure(1);
plot(t,sinewave);
xlabel('Time (s)')
ylabel('Amplitude')
title('Plot of sine wave from t = 0 to 10ms')
axis([0 10/1000 -1 1])

% Finally, play the sinewave you generated and verify.
soundsc(sinewave);

%% Exercise: Write a 1.5 seconds sine sweep from 400Hz to 800Hz.
% Plot the first 10ms in the top of a figure
% and the last 10ms in the bottom of the same figure
% use the command "subplot" to get more than 1 plot in the same figure

% some variables that you must have:
% t : vector of time
% freq : vector of frequency, same length as t
% sinesweep : output vector

% <----- Here is space for you to complete this section ----->

t = 0:1/8192:1.5;
freq = linspace(400,800,8192*1.5+1);
sinesweep = sin(2*pi*freq.*t);

figure(2)
subplot(2,1,1);
plot(t,sinesweep);
xlabel('Time (s)')
ylabel('Amplitude')
title('Plot of sine sweep from 400 Hz to 800 Hz (t=0 to t=10ms)')
axis([0 10/1000 -1 1])

subplot(2,1,2);
plot(t,sinesweep);
xlabel('Time (s)')
ylabel('Amplitude')
title('Plot of sine sweep from 400 Hz to 800 Hz (t=1.49s to t=1.5s)')
axis([1490/1000 1.5 -1 1])

soundsc(sinesweep);


%% More mathematical functions and Graphing tools

% The following are some 2D graphing tools that you may try:
% plot, bar, stairs, errorbar, polar, stem, scatter

% The following are some 3D graphing tools that you may try:
% plot3, mesh, surf, surfl, contour, (quiver, slice)

% familiarize yourself with these commands and you will be asked about the
% function and description of one of the above commands during the demo.

%% Matrix and Vector Arithmetic

% Compute the value of A dot B and store in the variable C
A = [1 0];
B = [2, 3];	% commas are ok
C = dot(A,B)   % correct ans: 2.

% Compute the value of A cross C and store in the variable C
% Show that this makes sense in a 3D plot (hint: plot3)
A = [1 0 0];
B = [2 3 0];
C = cross(A,B);

%% Combination summary

% Do by hand:
% Write the characteristic polynomial of the following matrix [1,3; 4,5],
% Solve for the eigenvalues

% Here is how it can be done using MATLAB
A = [1, 3; 4, 5];
% characteristic polynomial:
% det([1,3; 4,5] - x[1,0;0,1]) = (1-x)(5-x) - (4)(3) = x^2 - 6x -7
polyA = poly(A);
% roots of characteristic polynomial are called eigenvalues:
% x^2 - 6x - 7 = 0; x = 7, x = -1
rootsA = roots(polyA);

%% Combination Exercise

% Repeat the above exercise for the following matrix

B = rand(5,5)

% In addition, plot the eigenvalues on the complex plane.
% Furthermore, draw the unit circle for reference.
% Use the command "hold on" to draw two plots on the same graph.

% <----- Here is space for you to complete this section ----->


polyB = poly(B)
rootsB = roots(polyB)
theta = linspace(0,2*pi)

figure(3)
scatter(real(rootsB),imag(rootsB))
hold on
plot(cos(theta),sin(theta))
hold off
axis([-3 3 -2 2])
axis equal
grid
title('Eigenvalues of 5x5 Matrix on the Complex Plane')
xlabel('Real')
ylabel('Imaginary')



%% Final example of the day

clear all; close all; clc;

% Most analog and digital filters (eg. audio filters, image filters) can be
% represented by a transfer function known as H(w). This will be covered in
% ECE210. H(w) is a complex-valued function with magnitude and phase. It is
% critical for most applications to determine the 3dB bandwidth. In the
% case of low pass filters, it is the angular frequency w_3dB where H(w) is
% half its peak value.

% In this exercise, modify the following code such that the magnitude is
% displayed in logarithmic power scale = 20*log_base10(|H(w)|), draw a
% horizontal line intersection |H(w)| at the 3dB point.

% Make a copy of the code before modification and be able to show us where
% you made the modification

w = 0:0.01:10;
H = 1./(1+1j*w);

figure(4)
subplot(2,1,1);
plot(w,abs(H)); 
title('Magnitude of H vs w');
xlabel('w');
ylabel('Magnitude'); 
grid on;

subplot(2,1,2);
plot(w,angle(H)); title('Phase of H vs w');
xlabel('w');
ylabel('Phase'); 
grid on;

% Modified code:
w = 0:0.01:10;
H = 1./(1+1j*w);

figure(5)
subplot(2,1,1);
plot(w,20*log10(abs(H)));
hold on
plot(w,ones(1001)*-3,'r--')
hold off
title('Magnitude of H vs w');
xlabel('w');
ylabel('Magnitude (dB)'); 
axis([0 10 -25 0])
grid on;

subplot(2,1,2);
plot(w,angle(H)); title('Phase of H vs w');
xlabel('w');
ylabel('Phase'); 
grid on;