import numpy as np
import matplotlib.pyplot as plt

t = np.arange(-6.28,6.28,0.01)
PI = np.pi


##################
# Question A, part 1:
f = 0.5 * np.cos(PI*t - PI/2) + 4/(3*PI) * np.cos(PI/2 * t)
for n in range(3,100,2):
    f += 4/(PI*(n**2 - 4)) * np.cos(n*PI/2 * t + PI)

fig_a, ax_a = plt.subplots(nrows=1, ncols=1)
# Show the x- and y-axis and the grid
ax_a.grid(True, which='both', linestyle='--')
ax_a.axvline(x=0, color='k', linewidth='1.0')
ax_a.axhline(y=0, color='k', linewidth='1.0')

ax_a.set_xlabel('t')
ax_a.set_ylabel('f(t)')
ax_a.set_title('Input f(t) vs. Time')

ax_a.plot(t, f, label='f(t)')
fig_a.tight_layout()


##################
# Question A, part 2:
def mag_phase_H(w):
    H = PI/2 * (1 + w**2)*np.exp(-1j*w)
    return np.absolute(H), np.angle(H)

w = np.linspace(-7*PI/4, 7*PI/4, 100)
H_w_mag, H_w_phase = mag_phase_H(w)

fig_b, ax_b = plt.subplots(nrows=2, ncols=1)
# Show the x- and y-axis and the grid
ax_b[0].grid(True, which='both', linestyle='--')
ax_b[0].axvline(x=0, color='k', linewidth='1.0')
ax_b[0].axhline(y=0, color='k', linewidth='1.0')

ax_b[0].set_xlabel('w')
ax_b[0].set_ylabel('|H(w)|')
ax_b[0].set_title('Amplitude Response |H(w)| vs Frequency')

ax_b[1].grid(True, which='both', linestyle='--')
ax_b[1].axvline(x=0, color='k', linewidth='1.0')
ax_b[1].axhline(y=0, color='k', linewidth='1.0')

ax_b[1].set_xlabel('w')
ax_b[1].set_ylabel('<H(w)')
ax_b[1].set_title('Phase Response <H(w) vs Frequency')

ax_b[0].plot(w, H_w_mag, label='|H(w)|')
ax_b[1].plot(w, H_w_phase, label='<H(w)')

fig_b.tight_layout()


##################
# Question A, part 4:
t = np.linspace(0,10,100)
H_mag__5, H_phase__5 = mag_phase_H(PI/2)
H_mag_1, H_phase_1 = mag_phase_H(PI)
H_mag_1_5, H_phase_1_5 = mag_phase_H(3*PI/2)
y_ss = H_mag_1 * 0.5*np.cos(PI * t - PI/2 + H_phase_1)
y_ss += H_mag__5 * 4/(3*PI)*np.cos(PI/2 * t + H_phase__5)
y_ss += H_mag_1_5 * 4/(5*PI)*np.cos(3*PI/2 * t + PI + H_phase_1_5)

fig_c, ax_c = plt.subplots(nrows=1,ncols=1)
ax_c.grid(True, which='both', linestyle='--')
ax_c.axvline(x=0, color='k', linewidth='1.0')
ax_c.axhline(y=0, color='k', linewidth='1.0')

ax_c.plot(t, y_ss, label='y_ss(t)')
ax_c.set_title('Steady-State Output y(t) vs. Time')
fig_c.tight_layout()


##################
# Question B:
fig_d, ax_d = plt.subplots(nrows=1,ncols=1)
y_corr = np.correlate(y_ss, y_ss, 'full')
y_corr = y_corr[y_corr.size//2:]

ax_d.plot(t, y_corr, 'b--')
ax_d.set_xlabel('t')
ax_d.set_ylabel('y_corr(t)')
ax_d.set_title('Output Auto-Correlation')

fig_d.tight_layout()

##################
# Part C:

# 628 = input timestep corresponding to t=0
# 828 = input timestep corresponding to t=2
# 0 = output timestep corresponding to t=0
# 20 = output timestep corresponding to t=2
input_power = sum(abs(f[628:828]))
output_power = sum(abs(y_ss[0:20]))

ratio = input_power/output_power
print('\nratio of input signal power to output signal power = {:.4f}'.format(ratio))


plt.show()
