function [ z_mag, z_phase ] = myHW5Qn5( w, F )
    % Print the cosine function for the input frequency, w, and phasor, F.
    % Return the magnetude and phase of the phasor as z_mag and z_phase
    
    z_mag = abs(F);
    z_phase = angle(F);
    
    disp(['For w = ', num2str(w), 'rad/s, F = ', num2str(F), ', Answer: ', num2str(z_mag),...
        'cos(', num2str(w), 't', num2str(z_phase, '%+.5g'), ')'])
    

end

