import matplotlib.pyplot as plt
import numpy as np

w = np.arange(0, 100, .01)

R = 1.0
L = 0.2
C = 0.05

H = 1 / (R + 1j*w*L + 1/(1j*w*C))
H_mag = 1 / np.sqrt(R**2 + (w*L + 1/(w*C))**2)


fig1, ax = plt.subplots(nrows = 3, ncols = 2)
ax1 = ax[0,0]
ax2 = ax[1,0]
ax3 = ax[2,0]
ax4 = ax[0,1]
ax5 = ax[1,1]
ax6 = ax[2,1]

ax1.plot(w, H_mag, label = '|H(w)|')
ax1.hold(True)
ax1.plot([10,10],[0,max(H_mag)], label = 'w_0=10 rad/s')

ax2.plot(w, np.real(H), label = 'Re{H(w)}')
ax2.hold(True)
ax2.plot([10,10],[0,1], label = 'w_0=10 rad/s')

ax3.plot(w, np.imag(H), label = 'Im{H(w)}')
ax3.hold(True)
ax3.plot([10,10],[-0.5, 0.5], label = 'w_0=10 rad/s')

handles1, labels1 = ax1.get_legend_handles_labels()
ax1.legend(handles1, labels1, loc = 'best')
handles2, labels2 = ax2.get_legend_handles_labels()
ax2.legend(handles2, labels2, loc = 'best')
handles3, labels3 = ax3.get_legend_handles_labels()
ax3.legend(handles3, labels3, loc = 'best')



L = 0.2
C = 0.05

R = 1.0
H_mag = 1 / np.sqrt(R**2 + (w*L + 1/(w*C))**2)
ax4.plot(w, H_mag, label = '|H(w)|, R=1')
ax4.hold(True)
ax4.plot([10,10],[0,max(H_mag)], label = 'w_0=10 rad/s')

R = 10.0
H_mag = 1 / np.sqrt(R**2 + (w*L + 1/(w*C))**2)
ax5.plot(w, H_mag, label = '|H(w)|, R=10')
ax5.hold(True)
ax5.plot([10,10],[0,max(H_mag)], label = 'w_0=10 rad/s')

R = 0.1
H_mag = 1 / np.sqrt(R**2 + (w*L + 1/(w*C))**2)
ax6.plot(w, H_mag, label = '|H(w)|, R=0.1')
ax6.hold(True)
ax6.plot([10,10],[0,max(H_mag)], label = 'w_0=10 rad/s')

handles4, labels4 = ax4.get_legend_handles_labels()
ax4.legend(handles4, labels4, loc = 'best')
handles5, labels5 = ax5.get_legend_handles_labels()
ax5.legend(handles5, labels5, loc = 'best')
handles6, labels6 = ax6.get_legend_handles_labels()
ax6.legend(handles6, labels6, loc = 'best')

fig1.tight_layout()

plt.show()
