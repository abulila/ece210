import matplotlib.pyplot as plt
import numpy as np

t = np.arange(0, 2*np.pi, .01)

f = np.real(4*np.exp(3j*t) + 4*np.exp(-3j*t))

fig1 = plt.figure(num = 1)
ax1 = fig1.gca()

ax1.plot(t, f, label = 'f(t)')

handles1, labels1 = ax1.get_legend_handles_labels()

ax1.legend(handles1, labels1, loc = 'best')
fig1.tight_layout()
plt.show()
