import matplotlib.pyplot as plt
import numpy as np

w = np.arange(0, 20, .01)

H = (1j*w) / (1 - w**2 + 2j*w)

fig1, ax = plt.subplots(nrows = 2)
ax1 = ax[0]
ax2 = ax[1]

ax1.plot(w, np.absolute(H), label = '|H(w)|')
ax2.plot(w, np.angle(H), label = '<H(w)')

handles1, labels1 = ax1.get_legend_handles_labels()
handles2, labels2 = ax2.get_legend_handles_labels()

ax1.legend(handles1, labels1, loc = 'best')
ax2.legend(handles2, labels2, loc = 'best')
fig1.tight_layout()
plt.show()
